#!/bin/bash

# normal compilation
# export CC=gcc-9
# export CXX=g++-9
rm -rf _build
mkdir _build
cd _build
cmake .. -DLIBMBUS_BUILD_EXAMPLES=ON
cmake --build . -j


# conan create . gocarlos/testing --build missing
